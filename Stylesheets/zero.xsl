<?xml version="1.0" encoding="UTF-8" ?>
<!--
    Created by Sergio Sicari
    Copyright (c) 2013 . All rights reserved.
-->
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:php="http://php.net/xsl"
                xmlns:dyn="http://exslt.org/dynamic"
                extension-element-prefixes="dyn"
                exclude-result-prefixes="php">
    <xsl:output encoding="utf-8" indent="yes" method="xml" />
    
    <xsl:template match="/">
      <events>
     	  <xsl:apply-templates />
      </events>
    </xsl:template>
    
    <xsl:template match="body">
      <xsl:variable name="org" select='div[@id="contengotutto"]/div[@id="main-content"]/div[@id="large-column"]/div/ul'/>
      <!-- <xsl:copy-of select="$org/div/ul" /> -->
      <xsl:for-each select='$org/li[@class="vevent first without-photo"]'>
      <event>
	<type>
		<xsl:value-of select="normalize-space(div/a/text())"/>
	</type>
	<title>
		<xsl:value-of select='normalize-space(div/h4/a[@class="summary title"])'/>
	</title>
	<start>
		<xsl:value-of select='normalize-space(div/span[@class="dtstart"])' />
	</start>
	<location>
		<xsl:value-of select='normalize-space(div/span[@class="venue"])' />
	</location>
      </event>
      </xsl:for-each>
    </xsl:template>

    <xsl:template match="text()" priority="-1" />

</xsl:stylesheet>
